<br />

![Chatbot](https://github.com/sourceduty/sourceduty/assets/123030236/aab991d0-58da-4e84-810d-ae623ef4e6d0)

## 📇 Sourceduty Repository Index
<br />

<details><summary>

### Python
</summary>

#### General

1. [Crazy_Mouse_Pointer](https://github.com/sourceduty/Crazy_Mouse_Pointer)
1. [Python Utilities](https://github.com/sourceduty/Python_Utilities)
1. [World Clocks Live Wallpaper](https://github.com/sourceduty/World-Clocks-Live-Wallpaper)
1. [Python_Math](https://github.com/sourceduty/Python_Math)
1. [Logic Gates](https://github.com/sourceduty/Logic)
1. [Python Decisions](https://github.com/sourceduty/Python_Decisions)
1. [Random Travel Generator](https://github.com/sourceduty/Random-Travel-Generator)
1. [YouTube Subtitle Analytics](https://github.com/sourceduty/YouTube-Subtitle-Analytics)
1. [Not Sure Slang Meme Thesaurus](https://github.com/sourceduty/Not_Sure_Slang_Meme_Thesaurus)
1. [Simple Chatbot](https://github.com/sourceduty/Simple_Chatbot)
1. [Highlighted_Map](https://github.com/sourceduty/Highlighted_Map)
1. [Text_File_Name_Suggestion](https://github.com/sourceduty/Text_File_Name_Suggestion)
1. [Machine_Learning_Template](https://github.com/sourceduty/Machine_Learning_Template)
1. [Text File Metadata](https://github.com/sourceduty/Text_File_Metadata)
1. [Python Metadata](https://github.com/sourceduty/Python_Metadata)
1. [Pyduino_Metadata](https://github.com/sourceduty/Pyduino_Metadata)
1. [Clock_Metadata](https://github.com/sourceduty/Clock_Metadata)
1. [Progress_Bar_Clock](https://github.com/sourceduty/Progress_Bar_Clock)

</details>
<details><summary>

### Arduino
</summary>

1. [PowerTime Logger](https://github.com/sourceduty/PowerTime) 
1. [Gun Target](https://github.com/sourceduty/ArduinoTarget)
1. [ScreenTime Break](https://github.com/sourceduty/ScreenTimeBreak)

</details>
<details><summary>

### Windows
</summary>

1. [Right-Click Converter](https://github.com/sourceduty/Right-Click_Converter)

</details>
<details><summary>

### Archcraft
</summary>

1. [Archcraft_Python](https://github.com/sourceduty/Archcraft_Python)
1. [Archcraft_Apps](https://github.com/sourceduty/Archcraft_Apps)

</details>
<details><summary>

### Concepts
</summary>

#### General

1. [Huge_Cursor](https://github.com/sourceduty/Huge_Cursor)
1. [Video_Speed_Compression](https://github.com/sourceduty/Video_Speed_Compression)
1. [Battery_Level_Cursor](https://github.com/sourceduty/Battery_Level_Cursor)
1. [SMS_Web_Search](https://github.com/sourceduty/SMS_Web_Search)
1. [Web_Download_Queue](https://github.com/sourceduty/Web_Download_Queue)
1. [Dormant_Music_Files](https://github.com/sourceduty/Dormant_Music_Files)
1. [Smart_Web_Search](https://github.com/sourceduty/Smart_Web_Search)
1. [Sequenced_File_Folders](https://github.com/sourceduty/Sequenced_File_Folders)
1. [AI Group Chat](https://github.com/sourceduty/AI-Group_Chat)
1. [AI_Group-Chat-Adviser](https://github.com/sourceduty/AI_Group-Chat-Adviser)
1. [Vehicle Route Analysis](https://github.com/sourceduty/Predictive_Route_Analysis)
1. [Business Blog](https://github.com/sourceduty/Business_Blog) 
1. [Regulated File Manager](https://github.com/sourceduty/Regulated_File_Manager)
1. [Chain Story](https://github.com/sourceduty/Chain_Story)
1. [Amazon_Premium](https://github.com/sourceduty/Amazon_Premium)
1. [Brand Footprint](https://github.com/sourceduty/Digital_Brand_Footprint)
1. [Dynamic_Text_Editor](https://github.com/sourceduty/Dynamic_Text_Editor)
1. [Windows_Deviance](https://github.com/sourceduty/Windows_Deviance)
1. [Multiuser Meeting Locator](https://github.com/sourceduty/Multiuser_Meeting_Locator)
1. [Repo_Card_Generator](https://github.com/sourceduty/Repo_Card_Generator)
1. [Contact_Verification](https://github.com/sourceduty/Contact_Verification)
1. [Voicemail-2-Text](https://github.com/sourceduty/Voicemail-2-Text)
1. [Serial_Simulator](https://github.com/sourceduty/Serial_Simulator)
1. [Trash_Capacity](https://github.com/sourceduty/Trash_Capacity)
1. [Advent Money App](https://github.com/sourceduty/Advent_Money)
1. [Recommended_Food](https://github.com/sourceduty/Recommended_Food)
1. [Cursor_Taskbar_Menu](https://github.com/sourceduty/Cursor_Taskbar_Menu)
1. [Encoded_Interactive_Graphic](https://github.com/sourceduty/Encoded_Interactive_Graphic_Format)

#### Unlikely

1. [Unlikely_Concept_Ideas](https://github.com/sourceduty/Unlikely_Concept_Ideas)

#### Peripheral

1. [Per-key RGB Keyboard](https://github.com/sourceduty/Per-key_RGB_Keyboard)
1. [Keybind_Instructions](https://github.com/sourceduty/Keybind_Instructions)

#### 3D Models

1. [Cults_3D](https://github.com/sourceduty/Cults_3D)
1. [Cura_Infocard](https://github.com/sourceduty/Cura_Infocard)

#### Canada.ca

1. [My Canada Account](https://github.com/sourceduty/My_Canada_Account)
1. [Ontario Parks](https://github.com/sourceduty/Ontario_Parks_Canada)
1. [Land Palette](https://github.com/sourceduty/Land_Palette)

#### ~~Twitter~~ X

1. [Tweet_Marketplace](https://github.com/sourceduty/Tweet_Marketplace)
1. [Twitter Preschedule](https://github.com/sourceduty/Tweet_Prescheduler)

#### Google

1. [YouTube_Automation](https://github.com/sourceduty/YouTube_Automation)
1. [YouTube_Video_Card](https://github.com/sourceduty/YouTube_Video_Card)
1. [Google_Images_Checkbox](https://github.com/sourceduty/Google_Images_Checkbox)
1. [YouTube_Totals](https://github.com/sourceduty/YouTube_Totals)
1. [Google_Earth_Dynamics](https://github.com/sourceduty/Google_Earth_Dynamics)
1. [Chrome_Context_Menu_Holder](https://github.com/sourceduty/Google_Chrome_Context_Menu_Holder)
1. [Video_Caption_Summary](https://github.com/sourceduty/Video_Caption_Summary)

</details>
<details><summary>

### Games
</summary>

#### Board

1. [Connect Four](https://github.com/sourceduty/Connect_Four)

#### Retro

1. [Sky Tetris](https://github.com/sourceduty/SkyTetris)
1. [Mario Airplane](https://github.com/sourceduty/RetroMarioAirplane)
1. [Wall Tetris](https://github.com/sourceduty/Wall-Tetris)
1. [Ocean Invaders](https://github.com/sourceduty/Ocean-Invaders)
1. [Retropie - Le Potato](https://github.com/sourceduty/Retropie_Le_Potato)

#### Concepts

1. [Kick Limit Soccer](https://github.com/sourceduty/Kick_Limit_Soccer_Game)
1. [Fortnite Storm Shield](https://github.com/sourceduty/Fortnite_Storm_Shield)
1. [Heart Shooting Gallery](https://github.com/sourceduty/heart-shooting-gallery)
1. [Fortnite Triple Royale](https://github.com/sourceduty/Fortnite_Triple_Royale)
1. [SMS Games](https://github.com/sourceduty/SMS_Games)
1. [Window_Rotation_Game](https://github.com/sourceduty/Window_Rotation_Game)

</details>
<details><summary>

### General
</summary> 

1. [Logic_Gate_Tree_Diagrams](https://github.com/sourceduty/Logic_Gate_Tree_Diagrams)
1. [Licenses](https://github.com/sourceduty/Licenses)
1. [Process_Theory](https://github.com/sourceduty/Process_Theory)
1. [PlayerCode Font](https://github.com/sourceduty/PlayerCode)
1. [Skynet](https://github.com/sourceduty/Skynet)
1. [Asimov_Laws](https://github.com/sourceduty/Asimov_Laws)
1. [Popular Perfection](https://github.com/sourceduty/Popular_Perfection)

</details>
<details><summary>

### Discussion

</summary>
  
1. [Sourceduty Repositories](https://github.com/sourceduty/sourceduty/discussions/1)

</details>
<details><summary>
  
#

#### 🙋 [Community Introduction](https://github.com/orgs/community/discussions/23204#discussioncomment-6736129)

print('Hello, community!')
<br />

I'm Alex Aldridge from Canada. I'm a self-taught designer, fan artist and programmer. I recently created a company named Sourceduty which is where I work. Sourceduty shares open-source codes and concept ideas on GitHub. Sourceduty also designs 3D models, fan art and architecture.

#
#### ☁️ OneDrive [Portfolio](https://1drv.ms/u/s!AumZxqj6wFkfhxSi1JbL7tJmhDCR?e=Rp0Jnr)
#
[![Sourceduty's GitHub Activity Graph](https://github-readme-activity-graph.vercel.app/graph?username=sourceduty&theme=react-dark)](https://github.com/ashutosh00710/github-readme-activity-graph)
#
![image](https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=sourceduty)
#
